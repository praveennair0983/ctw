package com.example.ctw.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ctw.databinding.RowItem2Binding
import com.example.ctw.databinding.RowItemBinding
import com.example.ctw.model.Article
import com.example.ctw.utils.Constants.CARDVIEW_FLAVOR


class NewsListAdapter(
    private val flavor: Int,
    private val articleList: MutableList<Article>,
    private val onItemClicked: (position: Int) -> Unit
) : RecyclerView.Adapter<NewsListAdapter.NewsViewHolder>() {

    private lateinit var rowItemBinding: RowItemBinding
    private lateinit var rowItem2Binding: RowItem2Binding

    class NewsViewHolder(view: View, private val onItemClicked: (position: Int) -> Unit) :
        RecyclerView.ViewHolder(view), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            val position = adapterPosition
            onItemClicked(position)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NewsViewHolder {
        // Create a new view, which defines the UI of the list item

        if (flavor == CARDVIEW_FLAVOR) {
            rowItem2Binding = RowItem2Binding.inflate(LayoutInflater.from(viewGroup.context))
            return NewsViewHolder(rowItem2Binding.root, onItemClicked)
        } else
            rowItemBinding = RowItemBinding.inflate(LayoutInflater.from(viewGroup.context))
        return NewsViewHolder(rowItemBinding.root, onItemClicked)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: NewsViewHolder, position: Int) {
        setBindingData(position)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = articleList.size

    private fun setBindingData(position: Int) {

        val publishedDate = articleList[position].publishedAt?.substring(5, 10)
        val publishedTime = articleList[position].publishedAt?.substring(11, 16)

        if (flavor == CARDVIEW_FLAVOR) {
            rowItem2Binding.titleView.text = articleList[position].title
            Glide.with(rowItem2Binding.root).load(articleList[position].urlToImage)
                .into(rowItem2Binding.newsImage)
            rowItem2Binding.date.text = ("$publishedDate $publishedTime")
        } else {
            rowItemBinding.headlineView.text = articleList[position].title
            Glide.with(rowItemBinding.root).load(articleList[position].urlToImage)
                .into(rowItemBinding.headlineImage)
            rowItemBinding.dateView.text = ("$publishedDate $publishedTime")
        }
    }

}