package com.example.ctw.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject


class ErrorResponse @Inject constructor() {

    @ActivityScoped
    @Inject
    lateinit var gson: Gson

    @SerializedName("message")
    var message: String? = null

    fun getErrorMsg(jsonMessage: String): ErrorResponse {
        return gson.fromJson(jsonMessage, this.javaClass)
    }
}