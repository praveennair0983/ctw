package com.example.ctw.model


import com.example.ctw.utils.Constants.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton


class RetrofitService @Inject constructor() {

    @Singleton
    @Inject
    lateinit var api: NewsApi

    init {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    suspend fun getTopHeadlineNews(sources: String, pageNumber: Int) =
        api.getTopHeadlineNews(sources, pageNumber)
}



