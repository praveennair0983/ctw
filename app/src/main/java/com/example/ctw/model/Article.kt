package com.example.ctw.model

import java.io.Serializable


data class Article(
    val source: Source?,
    val author: String?,
    val title: String?,
    val description: String?,
    val content: String?,
    val url: String?,
    val urlToImage: String?,
    val publishedAt: String?,

    ) : Serializable

