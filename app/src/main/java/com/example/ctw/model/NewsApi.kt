package com.example.ctw.model

import com.example.ctw.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface NewsApi {

    @GET("v2/top-headlines")
    suspend fun getTopHeadlineNews(
        @Query("sources")
        sources: String,
        @Query("page")
        pageNumber: Int = 1,
        @Query("apiKey")
        apiKey: String = Constants.API_KEY1
    ): Response<NewsResponse>

}