package com.example.ctw.viewmodel

import android.app.Application
import android.app.KeyguardManager
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.CancellationSignal
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.ctw.utils.Constants.AUTHENTICATION_CANCELLED
import com.example.ctw.utils.Constants.LOGIN_FAIL
import com.example.ctw.utils.Constants.LOGIN_SUCCEEDED
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class BiometricLoginViewModel @Inject constructor(application: Application) :
    AndroidViewModel(application) {

    private val loginResult = MutableLiveData<Int>()
    val rLoginResult: LiveData<Int>
        get() = loginResult

    private val loginFailMessage = MutableLiveData<String>()
    val rLoginFailMessage: LiveData<String>
        get() = loginFailMessage

    private val authenticationCallback: BiometricPrompt.AuthenticationCallback
        get() =
            @RequiresApi(Build.VERSION_CODES.P)
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                    super.onAuthenticationError(errorCode, errString)

                    if (errorCode == AUTHENTICATION_CANCELLED)
                        promptBiometricLogin()
                    else
                        loginResult.value = LOGIN_FAIL
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
                    super.onAuthenticationSucceeded(result)
                    loginResult.value = LOGIN_SUCCEEDED
                }

            }

    private fun checkBiometricSupport(): Boolean {
        val context = getApplication<Application>().applicationContext
        val keyguardManager: KeyguardManager =
            context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        if (!keyguardManager.isKeyguardSecure) {
            return false
        }
        if (ActivityCompat.checkSelfPermission(
                context,
                android.Manifest.permission.USE_BIOMETRIC
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }
        return if (context.packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            true

        } else true

    }

    private fun getCancellationSignal(): CancellationSignal {
        return CancellationSignal()
    }

    @RequiresApi(Build.VERSION_CODES.P)
    fun promptBiometricLogin() {
        loginResult.value = 0
        loginFailMessage.value = ""
        if (checkBiometricSupport()) {
            val context = getApplication<Application>().applicationContext
            val biometricPrompt = BiometricPrompt.Builder(context)
                .setTitle("User Authentication Required")
                .setSubtitle(" ")
                .setNegativeButton(
                    "CANCEL",
                    context.mainExecutor,
                    { _, _ ->
                        loginResult.value = LOGIN_FAIL //User Cancelled
                    }).build()
            biometricPrompt.authenticate(
                getCancellationSignal(),
                context.mainExecutor,
                authenticationCallback
            )
        } else
            loginResult.value = LOGIN_SUCCEEDED
    }

}