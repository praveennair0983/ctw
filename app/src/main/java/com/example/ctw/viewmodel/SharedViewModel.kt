package com.example.ctw.viewmodel

import androidx.lifecycle.ViewModel
import com.example.ctw.utils.Constants.DEFAULT_FLAVOR


class SharedViewModel : ViewModel() {

    private var newsViewTitle = ""
    private var newsSource = ""
    private var viewFlavor = DEFAULT_FLAVOR

    fun setNewsSourceData(title: String, flavor: Int, sourceName: String) {
        newsViewTitle = title
        viewFlavor = flavor
        newsSource = sourceName
    }

    fun getNewsTitle(): String {
        return newsViewTitle
    }

    fun getNewsSource(): String {
        return newsSource
    }

    fun getViewFlavor(): Int {
        return viewFlavor
    }
}