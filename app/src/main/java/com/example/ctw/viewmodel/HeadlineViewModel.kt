package com.example.ctw.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ctw.model.NewsResponse
import com.example.ctw.model.RetrofitService
import com.example.ctw.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton


@HiltViewModel
class HeadlineViewModel @Inject constructor() : ViewModel() {

    private val headlineNews = MutableLiveData<Resource<NewsResponse>>()
    private var headlineNewsPage = 1
    private var headlineNewsResponse: NewsResponse? = null

    @Singleton
    @Inject
    lateinit var service: RetrofitService

    val rHeadlineNews: LiveData<Resource<NewsResponse>>
        get() = headlineNews

    fun getHeadlineNews(newsSource: String) = viewModelScope.launch {
        headlineNews.postValue(Resource.Loading())
        headlineNews.postValue(
            handleHeadlineNews(
                service.getTopHeadlineNews(
                    newsSource, headlineNewsPage
                )
            )
        )
    }

    private fun handleHeadlineNews(response: Response<NewsResponse>): Resource<NewsResponse> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                headlineNewsPage++
                if (headlineNewsResponse == null) {
                    headlineNewsResponse = resultResponse
                } else {
                    val oldArticles = headlineNewsResponse?.articles
                    val newArticles = resultResponse.articles
                    oldArticles?.addAll(newArticles)
                }
                return Resource.Success(headlineNewsResponse ?: resultResponse)
            }
        }

        return Resource.Error(response.errorBody()!!.string())
    }

}