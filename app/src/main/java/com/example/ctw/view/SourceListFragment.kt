package com.example.ctw.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.ctw.R
import com.example.ctw.databinding.FragmentSourceListBinding
import com.example.ctw.utils.Constants.CARDVIEW_FLAVOR
import com.example.ctw.utils.Constants.DEFAULT_FLAVOR
import com.example.ctw.utils.ImageLoader
import com.example.ctw.utils.Utils
import com.example.ctw.viewmodel.SharedViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject


@AndroidEntryPoint
class SourceListFragment : Fragment(), View.OnClickListener {

    @FragmentScoped
    @Inject
    lateinit var imageLoader: ImageLoader

    @FragmentScoped
    @Inject
    lateinit var utils: Utils

    private var _binding: FragmentSourceListBinding? = null
    private val binding get() = _binding!!


    private val sharedModel: SharedViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        _binding = FragmentSourceListBinding.inflate(inflater, container, false)
        viewBindings()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClick(p0: View?) {
        setNewsSourceData(getNewsDomain(p0))
        Navigation.findNavController(binding.root).navigate(
            R.id.action_sourceListFragment_to_headlineFragment
        )
    }

    private fun viewBindings() {
        binding.apply {
            bbcnewsLogo.loadNewsLogo()
            espnLogo.loadNewsLogo()
            bbcnews.setOnClickListener(this@SourceListFragment)
            espn.setOnClickListener(this@SourceListFragment)
        }

    }

    private fun getNewsDomain(view: View?): String {
        return when (view) {
            binding.bbcnews, binding.bbcnewsLogo -> getString(R.string.bbc_news)
            binding.espn, binding.espnLogo -> getString(R.string.espn)
            else -> ""
        }
    }

    private fun setDomainTitle(domain: String): String {
        return when (domain) {
            getString(R.string.bbc_news) -> getString(R.string.title_bbc_news)
            getString(R.string.espn) -> getString(R.string.title_espn)
            else -> ""
        }
    }

    private fun setViewFlavor(domain: String): Int {
        return when (domain) {
            getString(R.string.bbc_news) -> DEFAULT_FLAVOR
            getString(R.string.espn) -> CARDVIEW_FLAVOR
            else -> DEFAULT_FLAVOR
        }
    }

    private fun ImageView.loadNewsLogo() {
        imageLoader.loadImage(this, utils.getImageUrlName(getNewsDomain(this)))
    }

    private fun setNewsSourceData(source: String) {
        sharedModel.setNewsSourceData(setDomainTitle(source), setViewFlavor(source), source)
    }

}