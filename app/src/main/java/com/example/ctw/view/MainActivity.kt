package com.example.ctw.view

import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.example.ctw.R
import com.example.ctw.utils.Constants
import com.example.ctw.utils.Constants.LOGIN_FAIL
import com.example.ctw.utils.Constants.LOGIN_FAIL_MESSAGE
import com.example.ctw.utils.Constants.LOGIN_SUCCEEDED
import com.example.ctw.viewmodel.BiometricLoginViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: BiometricLoginViewModel by viewModels()

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            processloginOutput()
            viewModel.promptBiometricLogin()
        }
        setNavigationGraph()
    }

    private fun setNavigationGraph() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        val navController = navHostFragment.navController

        val navGraph = navController.navInflater.inflate(R.navigation.navigation_graph)
        navGraph.setStartDestination(R.id.sourceListFragment)
        navController.graph = navGraph
    }

    private fun processloginOutput() {
        viewModel.rLoginResult.observe(this,
            { result ->
                when (result) {
                    LOGIN_SUCCEEDED -> setNavigationGraph()
                    LOGIN_FAIL_MESSAGE -> {
                        viewModel.rLoginFailMessage.observe(this, { message ->
                            Constants.customToastDisplay(message, this, 40, Constants.LENGTH_SHORT)
                        })
                        finish()
                    }
                    LOGIN_FAIL -> finish()
                }
            })
    }


}
