package com.example.ctw.view


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.example.ctw.R
import com.example.ctw.adapter.NewsListAdapter
import com.example.ctw.databinding.FragmentHeadlineBinding
import com.example.ctw.model.Article
import com.example.ctw.model.ErrorResponse
import com.example.ctw.model.NewsResponse
import com.example.ctw.utils.Constants
import com.example.ctw.utils.Resource
import com.example.ctw.viewmodel.HeadlineViewModel
import com.example.ctw.viewmodel.SharedViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject


/**
 * Headline Fragment.
 */
@AndroidEntryPoint
class HeadlineFragment : Fragment() {

    private var _binding: FragmentHeadlineBinding? = null
    private val binding: FragmentHeadlineBinding get() = _binding!!
    private val viewModel: HeadlineViewModel by viewModels()
    private val sharedModel: SharedViewModel by activityViewModels()
    private val tAg = "HeadlineFragment"

    @ActivityScoped
    @Inject
    lateinit var errorResponse: ErrorResponse

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHeadlineBinding.inflate(inflater, container, false)
        viewModel.rHeadlineNews.observe(viewLifecycleOwner, { result -> processOutput(result) })
        viewModel.getHeadlineNews(sharedModel.getNewsSource())

        binding.textView.text = sharedModel.getNewsTitle()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun onListItemClick(position: Int, articles: MutableList<Article>) {
        val bundle = Bundle().apply {
            putSerializable("article", articles[position])
        }
        Navigation.findNavController(binding.root).navigate(
            R.id.action_headlineFragment_to_articleFragment, bundle
        )
    }

    private fun processOutput(result: Resource<NewsResponse>) {
        when (result) {
            is Resource.Success<NewsResponse> -> {
                result.data?.let { newsResponse ->
                    binding.listView.adapter =
                        NewsListAdapter(
                            sharedModel.getViewFlavor(),
                            newsResponse.articles
                        ) { position: Int ->
                            onListItemClick(position, newsResponse.articles)
                        }
                    binding.listView.layoutManager = GridLayoutManager(activity, 1)
                }
            }
            is Resource.Error -> {
                result.message?.let { message ->
                    Constants.customToastDisplay(
                        errorResponse.getErrorMsg(message).message.toString(),
                        this.requireContext(),
                        40,
                        Constants.LENGTH_LONG
                    )
                    Log.e(tAg, "An error occurred: $message")
                }
                Navigation.findNavController(binding.root).navigate(
                    R.id.action_headlineFragment_to_sourceListFragment
                )
            }
            is Resource.Loading -> {
                result.message?.let { message ->
                    Constants.customToastDisplay(
                        errorResponse.getErrorMsg(message).message.toString(),
                        this.requireContext(),
                        40,
                        Constants.LENGTH_LONG
                    )
                    Log.e(tAg, "loading : $message")
                    Navigation.findNavController(binding.root).navigate(
                        R.id.action_headlineFragment_to_sourceListFragment
                    )
                }
            }
        }
    }
}



