package com.example.ctw.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.ctw.R
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject


class ImageLoader @Inject constructor(@ActivityContext private val context: Context) {


    private val requestOptions = RequestOptions
        .placeholderOf(R.drawable.ic_loading)
        .error(R.drawable.ic_error)
        .centerCrop()

    fun loadImage(target: ImageView, imageUrl: String) {
        Glide.with(context).load(imageUrl).apply(requestOptions).into(target)
    }


}