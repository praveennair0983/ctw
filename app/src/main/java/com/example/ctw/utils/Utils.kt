package com.example.ctw.utils

import android.content.Context
import com.example.ctw.R
import dagger.hilt.android.qualifiers.ActivityContext
import javax.inject.Inject

class Utils @Inject constructor(@ActivityContext private val context: Context) {


    fun getImageUrlName(domain: String): String {
        return when (domain) {
            getString(R.string.espn) -> "https://images-na.ssl-images-amazon.com/images/I/21h-OE4-X7L._SY355_.png"
            getString(R.string.bbc_news) -> "https://seeklogo.com/images/B/bbc-news-logo-E3DECDA65A-seeklogo.com.png"
            else -> ""
        }
    }

    private fun getString(id: Int): String {
        return context.getString(id)
    }

}