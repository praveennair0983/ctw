package com.example.ctw.utils

import android.content.Context
import android.graphics.Color
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.text.style.ForegroundColorSpan
import android.widget.Toast

object Constants {

    const val LOGIN_SUCCEEDED = 1
    const val LOGIN_FAIL_MESSAGE = 2
    const val LOGIN_FAIL = 3
    const val AUTHENTICATION_CANCELLED = 10
    const val LENGTH_LONG = Toast.LENGTH_LONG
    const val LENGTH_SHORT = Toast.LENGTH_SHORT
    const val DEFAULT_FLAVOR = 1
    const val CARDVIEW_FLAVOR = 2

    const val BASE_URL = "https://newsapi.org"
    const val API_KEY1 = "7780758ca2b54336bd115516a524fdde"

    fun customToastDisplay(
        message: String,
        context: Context?,
        size: Int,
        ToastLength: Int
    ) {
        val spannableString = SpannableString(message)
        spannableString.setSpan(
            ForegroundColorSpan(Color.RED), 0, spannableString.length, 0
        )
        spannableString.setSpan(
            AbsoluteSizeSpan(size), 0, spannableString.length, 0
        )
        val toast = Toast.makeText(context, spannableString, ToastLength)
        toast.show()
    }
}